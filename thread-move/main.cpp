#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_worker(unsigned int id, chrono::milliseconds interval)
{
    cout << "Start THD#" << id << endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        this_thread::sleep_for(interval);
    }

    cout << "End of THD#" << id << endl;
}

thread create_thread(chrono::milliseconds interval)
{
    static int id_gen = 0;

    return thread{&background_worker, ++id_gen, interval};
}

int main()
{
    cout << "processor count: "
         << max(1u, thread::hardware_concurrency()) << endl;

    thread thd1 = create_thread(100ms);
    thread thd2 = move(thd1);

    vector<thread> thds;

    thds.push_back(move(thd2));
    thds.push_back(create_thread(200ms));
    thds.push_back(create_thread(300ms));
    thds.emplace_back(&background_worker, 4, 150ms);

    for(auto& thd : thds)
        thd.join();
}
