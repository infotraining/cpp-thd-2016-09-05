#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <mutex>

using namespace std;

timed_mutex mtx;

void background_work(unsigned int id, chrono::milliseconds timeout)
{
    cout << "THD#" << id << " is waiting for mutex..." << endl;

    unique_lock<timed_mutex> lk{mtx, try_to_lock};

    if (!lk.owns_lock())
    {
        do
        {
            cout << "THD#" << id << "doesn't own a lock..."
                 << " Attempt to acquire mutex..." << endl;
        } while(!lk.try_lock_for(timeout));
    }

    cout << "Start THD#" << id <<endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << " is working..." << endl;
        this_thread::sleep_for(1s);
    }

    cout << "End of THD#" << id << endl;
}

int main()
{
    thread thd1{&background_work, 1, 500ms};
    thread thd2{&background_work, 2, 750ms};
    thd1.join();
    thd2.join();
}
