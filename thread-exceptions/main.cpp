#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <exception>

using namespace std;

void may_throw(unsigned int id, int arg)
{
    if (id % 2 == 0 && arg == 13)
        throw runtime_error("Error#13 from THD#" + to_string(id));

    if (arg == 23)
        throw out_of_range("Out of range from THD#" + to_string(id));
}

void background_worker(unsigned int id, unsigned int count,
                       chrono::milliseconds interval, exception_ptr& eptr)
{
    try
    {
        cout << "Start THD#" << id << endl;

        for(unsigned int i = 0; i < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            this_thread::sleep_for(interval);

            may_throw(id, i);
        }

        cout << "End of THD#" << id << endl;
    }
    catch(...)
    {
        cout << "Caught exception in THD#" << id << endl;
        eptr = current_exception();
    }
}

int main()
{
    exception_ptr eptr;
    thread thd1{&background_worker, 1, 20, 100ms, ref(eptr)};
    thd1.join();

    if (eptr)
    {
        try
        {
            rethrow_exception(eptr);
        }
        catch(const runtime_error& e)
        {
            cout << "Caught an exception in main: " << e.what() << endl;
        }
    }

    cout << "\n\n";

    const unsigned int no_of_threads = 4;

    vector<thread> thds(no_of_threads);
    vector<exception_ptr> thd_excpts(no_of_threads);

    for(unsigned int i = 0; i < no_of_threads; ++i)
        thds[i] = thread{&background_worker, i+1, (i+1) * 10, 100ms, ref(thd_excpts[i])};

    for(auto& t : thds)
        t.join();

    // error handling
    for(auto& eptr : thd_excpts)
    {
        if (eptr)
        {
            try
            {
                rethrow_exception(eptr);
            }
            catch (const runtime_error& e)
            {
                cout << "Caught an exception: " << e.what() << endl;
            }
            catch(const out_of_range& e)
            {
                cout << "Caught an out of range excpetion: " << e.what() << endl;
            }
        }
    }


}
