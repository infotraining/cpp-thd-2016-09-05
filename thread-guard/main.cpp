#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>

using namespace std;

void background_worker(unsigned int id, chrono::milliseconds interval)
{
    string text = "Hello world!";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "End of THD#" << id << endl;
}


void may_throw()
{
    random_device rd;
    mt19937_64 rnd_gen{rd()};
    uniform_int_distribution<> dist(0, 100);


    if (dist(rnd_gen) % 2 == 0)
        throw runtime_error("#13");
}

namespace util
{

    namespace thread_ref
    {
        class ThreadGuard
        {
            thread& thd_;
        public:
            ThreadGuard(thread& thd) : thd_{thd}
            {}

            ThreadGuard(const ThreadGuard&) = delete;
            ThreadGuard& operator=(const ThreadGuard&) = delete;

            ~ThreadGuard()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };

    }

    inline namespace thread_move
    {
        class ThreadGuard
        {
            thread thd_;
        public:
            ThreadGuard(thread&& thd) : thd_{move(thd)}
            {}

            ThreadGuard(const ThreadGuard&) = delete;
            ThreadGuard& operator=(const ThreadGuard&) = delete;

            ThreadGuard(ThreadGuard&&) = default;
            ThreadGuard& operator=(ThreadGuard&&) = default;

            thread& get()
            {
                return thd_;
            }

            ~ThreadGuard()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };

    }

    class RaiiThread
    {
        thread thd_;
    public:
        template <typename... Args>
        RaiiThread(Args&&... args) : thd_{forward<Args>(args)...}
        {
        }

        RaiiThread(const RaiiThread&) = delete;
        RaiiThread& operator=(const RaiiThread&) = delete;

        RaiiThread(RaiiThread&&) = default;
        RaiiThread& operator=(RaiiThread&&) = default;

        thread& get()
        {
            return thd_;
        }

        ~RaiiThread()
        {
            if (thd_.joinable())
                thd_.join();
        }
    };

    class DetachedThread
    {
        thread thd_;
    public:
        template <typename... Args>
        DetachedThread(Args&&... args) : thd_{forward<Args>(args)...}
        {
        }

        DetachedThread(const DetachedThread&) = delete;
        DetachedThread& operator=(const DetachedThread&) = delete;

        DetachedThread(DetachedThread&&) = default;
        DetachedThread& operator=(DetachedThread&&) = default;

        thread& get()
        {
            return thd_;
        }

        ~DetachedThread()
        {
            if (thd_.joinable())
                thd_.detach();
        }
    };
}

int main() try
{        
    thread thd1{&background_worker, 1, 200ms};
    util::ThreadGuard thd1_guard{move(thd1)};

    util::RaiiThread thd2{&background_worker, 2, 200ms}; // will join in destuctor
    util::RaiiThread thd2_moved = move(thd2);

    util::DetachedThread thd3{&background_worker, 3, 400ms}; // will detach in destructor

    may_throw();
}
catch(const exception& e)
{
    cout << "Caught exception: " << e.what() << endl;
}
