#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>
#include "thread_safe_queue.hpp"

using namespace std;

using Task = std::function<void()>;

class ThreadPool
{
    std::vector<std::thread> threads_;
    ThreadSafeQueue<Task> tasks_;
    const nullptr_t end_of_work{};

public:
    ThreadPool(size_t size) : threads_{size}
    {
        for(size_t i = 0; i <size; ++i)
            threads_[i] = std::thread{ [this] { run(); } };
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ThreadPool(ThreadPool&&) = delete;
    ThreadPool& operator=(ThreadPool&&) = delete;

    ~ThreadPool()
    {
        // sending poisoning pills
        for(size_t i = 0; i < threads_.size(); ++i)
            tasks_.push(end_of_work);

        for(auto& t : threads_)
            t.join();
    }

    template <typename Callable>
    auto submit(Callable&& task) -> future<decltype(task())>
    {
        using result_type = decltype(task());

        auto pt = make_shared<packaged_task<result_type()>>(forward<Callable>(task));
        auto future_result = pt->get_future();

        tasks_.push([pt] { (*pt)();});

        return future_result;
    }

private:
    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);

            if (task == end_of_work)
                return;

            task();
        }
    }
};

void background_work(int id)
{
    cout << "BW#" << id << " has started..." << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(500, 3000);

    auto time_interval = rnd_distr(rnd_gen);

    this_thread::sleep_for(chrono::milliseconds(time_interval));

    cout << "BW#" << id << " is finished..." << endl;
}

int calculate_square(int x)
{
    cout << "Starting calculation for " << x << " in thread_id:"
         << this_thread::get_id() << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(500, 3000);

    auto time_interval = rnd_distr(rnd_gen);

    this_thread::sleep_for(chrono::milliseconds(time_interval));

    if (x == 13)
        throw invalid_argument("Error#13");

    return x * x;
}

int main()
{
    ThreadPool thread_pool{8};

    thread_pool.submit([] { background_work(1); });
    thread_pool.submit([] { background_work(2); });

    for(int i = 3; i <= 20; ++i)
        thread_pool.submit([i] { background_work(i);});

    vector<future<int>> futures;

    for(int i = 100; i <= 120; ++i)
    {
        futures.push_back(
                    thread_pool.submit([i] { return calculate_square(i);}));

    }

    cout << "\n\nResults: " << endl;

    for(auto& f : futures)
        cout << f.get() << endl;
}
