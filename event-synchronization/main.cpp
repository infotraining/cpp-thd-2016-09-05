#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <atomic>

using namespace std;

namespace atomic_impl
{

    class Data
    {
        vector<int> data_;
        atomic<bool> is_ready_{};

    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            cout << "End of reading..." << endl;

            //is_ready_.store(true, memory_order::memory_order_release); // relaxed semantics - ok
            is_ready_ = true;  // is_ready_.store(true, memory_order::memory_order_seq_cst);
        }

        void process(int id)
        {
            //while(!is_ready_.load(memory_order::memory_order_acquire)) // relaxed semantics - ok
            while(!is_ready_) // while(!is_ready_.load(memory_order::memory_order_seq_cst))
            {
            }

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }
    };

}

namespace cv_impl
{
    class Data
    {
        vector<int> data_;
        bool is_ready_ = false;
        mutex mtx_;
        condition_variable cv_ready_;

    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            cout << "End of reading..." << endl;

            // setting a flag
            unique_lock<mutex> lk{mtx_};
            is_ready_ = true;
            lk.unlock();

            cv_ready_.notify_all();  // notification for all waiting threads
        }

        void process(int id)
        {
            unique_lock<mutex> lk{mtx_};

            cv_ready_.wait(lk, [this] { return is_ready_; });

            lk.unlock();

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }
    };
}

namespace singleton_atomic_impl
{

    class Singleton
    {
        static atomic<Singleton*> instance_;
        static mutex mtx_instance_;

        Singleton()
        {
            cout << "Singleton()" << endl;
        }

    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton()
        {
            cout << "~Singleton()" << endl;
        }

        void do_stuff()
        {
            cout << "Singleton::do_stuff()" << endl;
        }

        static Singleton& instance()
        {
            if (!instance_.load())
            {
                lock_guard<mutex> lk{mtx_instance_};

                if (!instance_.load())
                {
                    //instance_ = new Singleton();
                    void* raw_mem = ::operator new(sizeof(Singleton)); // 1
                    new (raw_mem) Singleton(); // 2
                    instance_.store(static_cast<Singleton*>(raw_mem)); // 3 //xxx
                }
            }

            return *instance_;
        }
    };

    atomic<Singleton*> Singleton::instance_{};
    mutex Singleton::mtx_instance_;

}

namespace singleton_meyers
{
    class Singleton
    {
        Singleton()
        {
            cout << "Singleton()" << endl;
        }

    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton()
        {
            cout << "~Singleton()" << endl;
        }

        void do_stuff()
        {
            cout << "Singleton::do_stuff()" << endl;
        }

        static Singleton& instance()
        {
            static Singleton unique_instance;
            return unique_instance;
        }
    };
}

namespace singleton_smart_ptr
{
    class Singleton
    {
        static std::unique_ptr<Singleton> instance_;
        static std::once_flag init_flag_;

        Singleton()
        {
            cout << "Singleton()" << endl;
        }

    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton()
        {
            cout << "~Singleton()" << endl;
        }

        void do_stuff()
        {
            cout << "Singleton::do_stuff()" << endl;
        }

        static Singleton& instance()
        {
            std::call_once(init_flag_, [] { instance_.reset(new Singleton()); });

            return *instance_;
        }
    };

    std::unique_ptr<Singleton> Singleton::instance_;
    std::once_flag Singleton::init_flag_;
}

int main()
{
    using cv_impl::Data;

    Data data;

    thread thd_producer{[&data] { data.read(); }};
    thread thd_consumer1{[&data] { data.process(1); }};
    thread thd_consumer2{[&data] { data.process(2); }};

    thd_producer.join();
    thd_consumer1.join();
    thd_consumer2.join();

    using singleton_smart_ptr::Singleton;

    thread thd_s1 { [] {Singleton::instance().do_stuff();} };
    thread thd_s2 { [] {Singleton::instance().do_stuff();} };

    thd_s1.join();
    thd_s2.join();
}
