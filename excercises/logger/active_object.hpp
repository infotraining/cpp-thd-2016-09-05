#ifndef ACTIVE_OBJECT_HPP
#define ACTIVE_OBJECT_HPP

#include <functional>
#include <thread>
#include "thread_safe_queue.hpp"

using Task = std::function<void()>;

class ActiveObject
{
public:
    ActiveObject() : is_done_{false}
    {
        thread_ = std::thread{&ActiveObject::run, this};
    }

    ActiveObject(const ActiveObject&) = delete;
    ActiveObject& operator=(const ActiveObject&) = delete;

    ~ActiveObject()
    {
        send([&] { is_done_ = true; });

        thread_.join();
    }

    void send(Task task)
    {
        tasks_.push(task);
    }

private:
    ThreadSafeQueue<Task> tasks_;
    std::thread thread_;
    bool is_done_;

    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);
            task();

            if (is_done_)
                return;
        }
    }
};

#endif // ACTIVE_OBJECT_HPP
