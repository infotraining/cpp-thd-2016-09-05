#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <future>

using namespace std;

void calc_hits(unsigned long long n, unsigned long long& hits)
{
    random_device rd;
    mt19937_64 rnd_gen{rd()};
    uniform_real_distribution<double> rnd_distr(-1.0, 1.0);

    unsigned long long local_counter = 0;

    for(unsigned long long i = 0; i < n; ++i)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y <= 1.0)
            ++local_counter;
    }

    hits += local_counter;
}

unsigned long long calc_hits2(unsigned long long n)
{
    random_device rd;
    mt19937_64 rnd_gen{rd()};
    uniform_real_distribution<double> rnd_distr(-1.0, 1.0);

    unsigned long long local_counter = 0;

    for(unsigned long long i = 0; i < n; ++i)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y <= 1.0)
            ++local_counter;
    }

    return local_counter;
}

void calc_hits_with_atomic(unsigned long long n, atomic<unsigned long long>& hits)
{
    random_device rd;
    mt19937_64 rnd_gen{rd()};
    uniform_real_distribution<double> rnd_distr(-1.0, 1.0);

    for(unsigned long long i = 0; i < n; ++i)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y <= 1.0)
            hits.fetch_add(1, memory_order::memory_order_relaxed);
            //++hits;
    }
}

void calc_hits_with_mutex(unsigned long long n, unsigned long long& hits, std::mutex& mtx)
{
    random_device rd;
    mt19937_64 rnd_gen{rd()};
    uniform_real_distribution<double> rnd_distr(-1.0, 1.0);

    for(unsigned long long i = 0; i < n; ++i)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);

        if (x * x + y * y <= 1.0)
        {
            lock_guard<mutex> lk{mtx};

            ++hits;
        }
    }
}

double mc_pi_single_thread(unsigned long long n)
{
    unsigned long long hits = 0;

    calc_hits(n, hits);

    return 4.0 * static_cast<double>(hits) / n ;
}


double mc_pi_multi_thread_safe(unsigned long long n)
{
    const auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_threads_count);
    vector<unsigned long long> partial_hits(hardware_threads_count);

    auto no_of_attempts = n / hardware_threads_count;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
        threads[i] = thread{&calc_hits, no_of_attempts, ref(partial_hits[i])};

    for(auto& thd : threads)
        thd.join();

    auto hits = accumulate(partial_hits.begin(), partial_hits.end(), 0UL);

    return 4.0 * static_cast<double>(hits) / n ;
}

double mc_pi_multi_thread_unsafe(unsigned long long n)
{
    const auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_threads_count);
    unsigned long long hits = 0;

    auto no_of_attempts = n / hardware_threads_count;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
        threads[i] = thread{&calc_hits, no_of_attempts, ref(hits)};

    for(auto& thd : threads)
        thd.join();

    return 4.0 * static_cast<double>(hits) / n ;
}

double mc_pi_multi_thread_with_atomic(unsigned long long n)
{
    const auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_threads_count);
    atomic<unsigned long long> hits{};

    auto no_of_attempts = n / hardware_threads_count;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
        threads[i] = thread{&calc_hits_with_atomic, no_of_attempts, ref(hits)};

    for(auto& thd : threads)
        thd.join();

    return 4.0 * static_cast<double>(hits) / n ;
}

double mc_pi_multi_thread_with_mutex(unsigned long long n)
{
    const auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_threads_count);
    unsigned long long hits{};
    mutex mtx_hits;

    auto no_of_attempts = n / hardware_threads_count;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
        threads[i] = thread{&calc_hits_with_mutex, no_of_attempts, ref(hits), ref(mtx_hits)};

    for(auto& thd : threads)
        thd.join();

    return 4.0 * static_cast<double>(hits) / n ;
}

double mc_pi_with_futures(unsigned long long n)
{
    const auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    vector<future<unsigned long long>> partial_hits;

    for(size_t i = 0; i < hardware_threads_count; ++i)
        partial_hits.emplace_back(
                    std::async(std::launch::async, &calc_hits2, n / hardware_threads_count));

    return 4.0 * std::accumulate(partial_hits.begin(), partial_hits.end(), 0.0,
                                 [](auto& hits, auto& f) { return hits + f.get(); }) / n;
}

int main()
{
    const auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    cout << "cores count: " << hardware_threads_count << endl;

    const unsigned long long n = 30'000'000;

    auto start = chrono::high_resolution_clock::now();

    auto pi = mc_pi_single_thread(n);

    auto end = chrono::high_resolution_clock::now();

    cout << "Single thread pi = " << pi
         << " time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " ms" << endl;


    cout << "\n\n------------------------\n\n";

    start = chrono::high_resolution_clock::now();

    pi = mc_pi_multi_thread_safe(n);

    end = chrono::high_resolution_clock::now();

    cout << "Multithread pi = " << pi
         << " time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " ms" << endl;

    cout << "\n\n------------------------\n\n";

    start = chrono::high_resolution_clock::now();

    pi = mc_pi_multi_thread_unsafe(n);

    end = chrono::high_resolution_clock::now();

    cout << "Unsafe pi = " << pi
         << " time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " ms" << endl;

    cout << "\n\n------------------------\n\n";

    start = chrono::high_resolution_clock::now();

    pi = mc_pi_multi_thread_with_atomic(n);

    end = chrono::high_resolution_clock::now();

    cout << "Atomic pi = " << pi
         << " time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " ms" << endl;

    cout << "\n\n------------------------\n\n";

    start = chrono::high_resolution_clock::now();

    pi = mc_pi_multi_thread_with_mutex(n);

    end = chrono::high_resolution_clock::now();

    cout << "Mutex pi = " << pi
         << " time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " ms" << endl;

    cout << "\n\n------------------------\n\n";

    start = chrono::high_resolution_clock::now();

    pi = mc_pi_with_futures(n);

    end = chrono::high_resolution_clock::now();

    cout << "Futures pi = " << pi
         << " time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " ms" << endl;
}
