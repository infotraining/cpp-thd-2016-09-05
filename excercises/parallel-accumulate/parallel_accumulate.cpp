#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>
#include <future>

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    auto hardware_thread_count = std::max(1u, std::thread::hardware_concurrency());

    std::vector<std::future<T>> partial_results;

    size_t range_size = std::distance(first, last);
    size_t subrange_size = range_size / hardware_thread_count;

    It subrange_start = first;

    for(size_t i = 0; i < hardware_thread_count - 1; ++i)
    {
        It subrange_end = std::next(subrange_start, subrange_size);

        partial_results.push_back(
                    std::async(std::launch::async,
                               &std::accumulate<It, T>, subrange_start, subrange_end, T{}));

        subrange_start = subrange_end;
    }

    init += std::accumulate(subrange_start, last, T{});

    return std::accumulate(partial_results.begin(), partial_results.end(), init,
                           [](const T& item, std::future<T>& f) { return item + f.get();});
}


int main(int argc, char *argv[])
{
    const size_t SIZE = 50000000;

    typedef unsigned long long int ValueType;

    std::vector<ValueType> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     std::accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    {
        std::cout << "\nParallel\n";

        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     parallel_accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }
}
