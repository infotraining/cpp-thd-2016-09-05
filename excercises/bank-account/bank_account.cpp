#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    const int id_;
    double balance_;

    using mutex = std::recursive_mutex;

    mutable mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = ";

        double balance_snapshot;
        {
            std::lock_guard<mutex> lk{mtx_};
            balance_snapshot = balance_;
        }

        std::cout << balance_snapshot << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<mutex> lk_from{mtx_, std::defer_lock};
        std::unique_lock<mutex> lk_to{to.mtx_, std::defer_lock};

        std::lock(lk_from, lk_to); // avoiding deadlock

        balance_ -= amount;
        to.balance_ += amount;
    }

//    void transfer(BankAccount& to, double amount)
//    {
//        std::lock_guard<mutex> lk_from{mtx_};
//        std::lock_guard<mutex> lk_to{to.mtx_};

//        withdraw(amount);
//        to.deposit(amount);
//    }

    void withdraw(double amount)
    {
        std::lock_guard<mutex> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<mutex> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<mutex> lk{mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_deposit(unsigned int counter, BankAccount& ba, double amount)
{
    for(unsigned int i = 0; i < counter; ++i)
        ba.deposit(amount);
}

void make_withdraw(unsigned int counter, BankAccount& ba, double amount)
{
    for(unsigned int i = 0; i < counter; ++i)
        ba.withdraw(amount);
}

void make_transfer(unsigned int counter, BankAccount& from, BankAccount& to, double amount)
{
    for(unsigned int i = 0; i < counter; ++i)
        from.transfer(to, amount);
}


int main()
{
    using namespace std;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    cout << "\n-----------------------\n\n";

    constexpr long no_of_operations = 1'000'000;

    thread thd1{ &make_deposit, no_of_operations, ref(ba1), 1.0 };
    thread thd2{ &make_withdraw, no_of_operations, ref(ba1), 1.0 };

    thd1.join();
    thd2.join();

    ba1.print();
    ba2.print();

    cout << "\n-----------------------\n\n";

    thread thd3{&make_transfer, no_of_operations, ref(ba1), ref(ba2), 1.0};
    thread thd4{&make_transfer, no_of_operations, ref(ba2), ref(ba1), 1.0};
    thd3.join();
    thd4.join();

    ba1.print();
    ba2.print();

    cout << "\n-----------------------\n\n";

    unique_lock<BankAccount> lk{ba1}; // LOCK()
    ba1.deposit(100.0);
    ba1.withdraw(300);
    ba1.deposit(200);
    ba1.transfer(ba2, 300);
    lk.unlock();  // UNLOCK()
}
