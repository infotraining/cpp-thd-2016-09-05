#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>
#include <random>

using namespace std;

int calculate_square(int x)
{
    cout << "Starting calculation for " << x << " in thread_id:"
         << this_thread::get_id() << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(500, 3000);

    auto time_interval = rnd_distr(rnd_gen);

    this_thread::sleep_for(chrono::milliseconds(time_interval));

    if (x == 13)
        throw invalid_argument("Error#13");

    return x * x;
}

void save_to_file(const string& filename)
{
    cout << "Saving to file: " << filename << endl;
    this_thread::sleep_for(2s);
    cout << "File " << filename << " is saved..." << endl;
}

void may_throw(int x)
{
    if (x == 13)
        throw invalid_argument("Error#13");
}

class SquareCalculator
{
    promise<int> promise_;
public:
    void operator()(int x)
    {
        cout << "Starting calculation in SquareCalculator: " << x << endl;

        this_thread::sleep_for(2s);

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            promise_.set_exception(current_exception());

            return;
        }


        promise_.set_value(x * x);

    }

    future<int> get_future()
    {
        return promise_.get_future();
    }
};

template <typename Callable>
auto launch_async(Callable&& c)
{
    using result_type = decltype(c());

    packaged_task<result_type()> pt{move(c)};

    future<result_type> future_result = pt.get_future();

    thread thd{move(pt)};
    thd.detach();

    return future_result;
}

int main()
{
    // 1st way - packaged_task
    packaged_task<int()> pt1{ [] { return calculate_square(8); }};
    packaged_task<int(int)> pt2 { &calculate_square };
    packaged_task<void(string)> pt3{ &save_to_file };

    future<int> f1 = pt1.get_future();
    future<int> f2 = pt2.get_future();
    future<void> f3 = pt3.get_future();

    thread thd1{move(pt1)}; // async call
    thread thd2{move(pt2), 13};
    thread thd3{move(pt3), "file.dat" };

    thd1.detach();
    thd2.detach();
    thd3.detach();

    while (f1.wait_for(200ms) != future_status::ready)
    {
        cout << "main is waiting for a result in f1" << endl;
    }

    try
    {
        cout << "result for f1: " << f1.get() << endl;
        cout << "result for f2: " << f2.get() << endl;
        f3.wait();
    }
    catch (const invalid_argument& e)
    {
        cout << "Caught an exception: " << e.what() << endl;
    }

    // 2nd way - promise
    SquareCalculator calc;

    auto f4 = calc.get_future();

    thread thd4{ref(calc), 13};
    thd4.detach();

    try
    {
        cout << "result for f4: " << f4.get() << endl;
    }
    catch (const invalid_argument& e)
    {
        cout << "Caught an exception: " << e.what() << endl;
    }

    cout << "\n\n";

    // 3rd way - async

    //auto f5 = async(launch::async, &calculate_square, 23);

    vector<future<int>> futures(10);

    //futures.push_back(move(f5));

    for(size_t i = 0; i < 10; ++i)
        futures[i] = async(launch::async, [i]{ return calculate_square(i); });

    cout << "\n\nResults:" << endl;

    for(auto& result : futures)
    {
        try
        {
            cout << result.get() << endl;
        }
        catch (const invalid_argument& e)
        {
            cout << "Caught an exception: " << e.what() << endl;
        }
    }

    cout << "\n\n-------------------\n\n" << endl;

    launch_async([] { save_to_file("file1.txt"); });
    launch_async([] { save_to_file("file1.txt"); });
    launch_async([] { save_to_file("file1.txt"); });

    this_thread::sleep_for(5s);
}
