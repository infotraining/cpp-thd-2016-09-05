#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_worker(unsigned int id, chrono::milliseconds interval)
{
    string text = "Hello world!";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "End of THD#" << id << endl;
}

class BackgroundWorker
{
    const int id_;
    const string text_;
public:
    BackgroundWorker(int id, const string& text) : id_{id}, text_{text}
    {}

    void operator()(chrono::milliseconds interval) const
    {
        for(const auto& c : text_)
        {
            cout << "BW#" << id_ << ": " << c << endl;
            this_thread::sleep_for(interval);
        }

        cout << "End of BW#" << id_ << endl;
    }
};

template <typename Container>
void print(const Container& c, const string& prefix)
{
    cout << prefix << ": [ ";
    for(auto&& item : c)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    thread not_a_thread;

    cout << "id: " << not_a_thread.get_id() << endl;

    {
        thread thd1{&background_worker, 1, chrono::milliseconds(800)};
        thread thd2{&background_worker, 2, 400ms};

        BackgroundWorker bw1{1, "Background work"};
        thread thd3{bw1, 100ms};
        thread thd4{BackgroundWorker{2, "Text"}, 400ms};

        const vector<int> data = { 1, 2, 3, 4, 5, 7, 8, 9, 10 };
        vector<int> dest1(data.size());
        vector<int> dest2(data.size());

        thread thd5{ [&data, &dest1] { copy(data.begin(), data.end(), dest1.begin()); } };
        thread thd6{ [&data, &dest2] { copy(data.begin(), data.end(), dest2.begin()); } };

        thd1.detach();
        assert(!thd1.joinable());

        thd2.join();
        thd3.join();
        thd4.join();
        thd5.join();
        thd6.join();
        assert(!thd6.joinable());


        print(dest1, "dest1");
        print(dest2, "dest2");

    }

    cout << "After scope" << endl;
}
